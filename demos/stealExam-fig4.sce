mode(1);
// Steal Exam  example from "Quantitative Attack Tree Analysis: Stochastic Bounds and Numerical Analysis" (Fig. 4 )
// discrete input bounds generation 50 bins
[e1_inf,e1_sup]=atkTr_erlangBounds(3,4/24,100,0.5);
[e2_inf,e2_sup]=atkTr_erlangBounds(2,3/24,50,1);
[e3_inf,e3_sup]=atkTr_erlangBounds(1,4/24,50,1);
[e4_inf,e4_sup]=atkTr_erlangBounds(6,4/24,100,0.5);
[e5_inf,e5_sup]=atkTr_erlangBounds(4,3/24,100,0.5);
[e6_inf,e6_sup]=atkTr_erlangBounds(1,0.5/24,200,0.25);
[e7_inf,e7_sup]=atkTr_erlangBounds(4,3/24,100,0.5);
[e8_inf,e8_sup]=atkTr_erlangBounds(1,4/24,50,1);
[e9_inf,e9_sup]=atkTr_erlangBounds(1,10/24,50,1);
[e10_inf,e10_sup]=atkTr_erlangBounds(4,6/24,50,1);
[e11_inf,e11_sup]=atkTr_erlangBounds(4,2/24,100,0.5);
[e12_inf,e12_sup]=atkTr_erlangBounds(1,24/24,25,2);
// discrete input bounds generation 200 bins
[d1_inf,d1_sup]=atkTr_erlangBounds(3,4/24,100,2);
[d2_inf,d2_sup]=atkTr_erlangBounds(2,3/24,50,4);
[d3_inf,d3_sup]=atkTr_erlangBounds(1,4/24,50,4);
[d4_inf,d4_sup]=atkTr_erlangBounds(6,4/24,100,2);
[d5_inf,d5_sup]=atkTr_erlangBounds(4,3/24,100,2);
[d6_inf,d6_sup]=atkTr_erlangBounds(1,0.5/24,200,1);
[d7_inf,d7_sup]=atkTr_erlangBounds(4,3/24,100,2);
[d8_inf,d8_sup]=atkTr_erlangBounds(1,4/24,50,4);
[d9_inf,d9_sup]=atkTr_erlangBounds(1,10/24,50,4);
[d10_inf,d10_sup]=atkTr_erlangBounds(4,6/24,50,4);
[d11_inf,d11_sup]=atkTr_erlangBounds(4,2/24,100,2);
[d12_inf,d12_sup]=atkTr_erlangBounds(1,24/24,25,8);
// attack tree computation
s4_sup=atkTr_seq(atkTr_seq(d9_sup,atkTr_or(d10_sup,d11_sup)),d12_sup);
s4_inf=atkTr_seq(atkTr_seq(d9_inf,atkTr_or(d10_inf,d11_inf)),d12_inf);
s2_sup=atkTr_or(d4_sup,atkTr_or(d5_sup,d6_sup));
s2_inf=atkTr_or(d4_inf,atkTr_or(d5_inf,d6_inf));
s1_sup=atkTr_seq(atkTr_or(d1_sup,d2_sup),d3_sup);
s1_inf=atkTr_seq(atkTr_or(d1_inf,d2_inf),d3_inf);
s3_sup=atkTr_seq(d7_sup,d8_sup);
s3_inf=atkTr_seq(d7_inf,d8_inf);
s5_sup=atkTr_or(atkTr_or(s1_sup,s2_sup),atkTr_or(s3_sup,s4_sup));
s5_inf=atkTr_or(atkTr_or(s1_inf,s2_inf),atkTr_or(s3_inf,s4_inf));
t4_sup=atkTr_seq(atkTr_seq(e9_sup,atkTr_or(e10_sup,e11_sup)),e12_sup);
t4_inf=atkTr_seq(atkTr_seq(e9_inf,atkTr_or(e10_inf,e11_inf)),e12_inf);
t2_sup=atkTr_or(e4_sup,atkTr_or(e5_sup,e6_sup));
t2_inf=atkTr_or(e4_inf,atkTr_or(e5_inf,e6_inf));
t1_sup=atkTr_seq(atkTr_or(e1_sup,e2_sup),e3_sup);
t1_inf=atkTr_seq(atkTr_or(e1_inf,d2_inf),e3_inf);
t3_sup=atkTr_seq(e7_sup,e8_sup);
t3_inf=atkTr_seq(e7_inf,e8_inf);
t5_sup=atkTr_or(atkTr_or(t1_sup,t2_sup),atkTr_or(t3_sup,t4_sup));
t5_inf=atkTr_or(atkTr_or(t1_inf,t2_inf),atkTr_or(t3_inf,t4_inf));
// cumulative distribution computation
cumS5_inf=tlist(['distribution','state','probability'],s5_inf.state,cumsum(s5_inf.probability));
cumS5_sup=tlist(['distribution','state','probability'],s5_sup.state,cumsum(s5_sup.probability));
cumT5_inf=tlist(['distribution','state','probability'],t5_inf.state,cumsum(t5_inf.probability));
cumT5_sup=tlist(['distribution','state','probability'],t5_sup.state,cumsum(t5_sup.probability));
//plot (Fig. 4)
scf(2)
clf()
a=gca();
a.thickness=2;
plot2d2(cumS5_inf.state,cumS5_inf.probability,color('red'));
plot2d2(cumS5_sup.state,cumS5_sup.probability,color('green'));
plot2d2(cumT5_inf.state,cumT5_inf.probability,color('blue'));
plot2d2(cumT5_sup.state,cumT5_sup.probability,color('magenta'));
legends(['inf 200';'sup 200';'inf 50';'sup 50'],[color('red') color('green') color('blue') color('magenta')],opt="lr");
