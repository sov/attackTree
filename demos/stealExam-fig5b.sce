mode(1);
// Steal Exam with 200 bins example  from "Quantitative Attack Tree Analysis: Stochastic Bounds and Numerical Analysis" (Fig. 5 right)

// discrete input bounds generation
[d1_inf,d1_sup]=atkTr_erlangBounds(3,4/24,100,2);
[d2_inf,d2_sup]=atkTr_erlangBounds(2,3/24,50,4);
[d3_inf,d3_sup]=atkTr_erlangBounds(1,4/24,50,4);
[d4_inf,d4_sup]=atkTr_erlangBounds(6,4/24,100,2);
[d5_inf,d5_sup]=atkTr_erlangBounds(4,3/24,100,2);
[d6_inf,d6_sup]=atkTr_erlangBounds(1,0.5/24,200,1);
[d7_inf,d7_sup]=atkTr_erlangBounds(4,3/24,100,2);
[d8_inf,d8_sup]=atkTr_erlangBounds(1,4/24,50,4);
[d9_inf,d9_sup]=atkTr_erlangBounds(1,10/24,50,4);
[d10_inf,d10_sup]=atkTr_erlangBounds(4,6/24,50,4);
[d11_inf,d11_sup]=atkTr_erlangBounds(4,2/24,100,2);
[d12_inf,d12_sup]=atkTr_erlangBounds(1,24/24,25,8);
// attack tree computation
s4_sup=atkTr_seq(atkTr_seq(d9_sup,atkTr_or(d10_sup,d11_sup)),d12_sup);
s4_inf=atkTr_seq(atkTr_seq(d9_inf,atkTr_or(d10_inf,d11_inf)),d12_inf);
s2_sup=atkTr_or(d4_sup,atkTr_or(d5_sup,d6_sup));
s2_inf=atkTr_or(d4_inf,atkTr_or(d5_inf,d6_inf));
s1_sup=atkTr_seq(atkTr_or(d1_sup,d2_sup),d3_sup);
s1_inf=atkTr_seq(atkTr_or(d1_inf,d2_inf),d3_inf);
s3_sup=atkTr_seq(d7_sup,d8_sup);
s3_inf=atkTr_seq(d7_inf,d8_inf);
s5_sup=atkTr_or(atkTr_or(s1_sup,s2_sup),atkTr_or(s3_sup,s4_sup));
s5_inf=atkTr_or(atkTr_or(s1_inf,s2_inf),atkTr_or(s3_inf,s4_inf));
s5b_sup=atkTr_or(s1_sup,atkTr_or(s3_sup,s4_sup));
s5b_inf=atkTr_or(s1_inf,atkTr_or(s3_inf,s4_inf));
// cumulative distribution computation
cumS5_inf=tlist(['distribution','state','probability'],s5_inf.state,cumsum(s5_inf.probability));
cumS5_sup=tlist(['distribution','state','probability'],s5_sup.state,cumsum(s5_sup.probability));
cumS5b_inf=tlist(['distribution','state','probability'],s5b_inf.state,cumsum(s5b_inf.probability));
cumS5b_sup=tlist(['distribution','state','probability'],s5b_sup.state,cumsum(s5b_sup.probability));
//plot (Fig. 5 right)
scf(2)
clf()
a=gca();
a.thickness=2;
plot2d2(cumS5_inf.state,cumS5_inf.probability,color('red'));
plot2d2(cumS5_sup.state,cumS5_sup.probability,color('green'));
plot2d2(cumS5b_inf.state,cumS5b_inf.probability,color('blue'));
plot2d2(cumS5b_sup.state,cumS5b_sup.probability,color('magenta'));
legends(['inf';'sup';'Binf';'B sup'],[color('red') color('green') color('blue') color('magenta')],opt="lr");
