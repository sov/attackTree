mode(1);
// stuxnet 25 bins example from "Quantitative Attack Tree Analysis: Stochastic Bounds and Numerical Analysis" 
// discrete input bounds generation (Fig. 6 inputs numbered from left to right)
stacksize('max') ;
[d1_inf,d1_sup]=atkTr_erlangBounds(2,1,12.5,2);
[d2_inf,d2_sup]=atkTr_erlangBounds(1,1,12.5,2);
[d3_inf,d3_sup]=atkTr_erlangBounds(1,1,12.5,2);
[d4_inf,d4_sup]=atkTr_erlangBounds(2,1,12.5,2);
[d5_inf,d5_sup]=atkTr_erlangBounds(1,12,1.5625,16);
[d6_inf,d6_sup]=atkTr_erlangBounds(1,8,1.5625,16);
[d7_inf,d7_sup]=atkTr_erlangBounds(1,24,0.78125,32);
//[d7_inf,d7_sup]=atkTr_erlangBounds(1,24,1.5625,16);
[d8_inf,d8_sup]=atkTr_erlangBounds(1,1,12.5,2);
[d9_inf,d9_sup]=atkTr_erlangBounds(3,0.2,100,0.25);
[d10_inf,d10_sup]=atkTr_erlangBounds(3,0.1,100,0.25);
[d11_inf,d11_sup]=atkTr_erlangBounds(1,30,0.390625,64);
[d12_inf,d12_sup]=atkTr_erlangBounds(3,0.1,100,0.25);
[d13_inf,d13_sup]=atkTr_erlangBounds(1,1,12.5,2);
[d14_inf,d14_sup]=atkTr_erlangBounds(20,20,3.125,8);
// double rate collect data
[e10_inf,e10_sup]=atkTr_erlangBounds(3,0.2,100,0.25);
// attack tree computation
s1_sup=atkTr_seq(d1_sup, atkTr_seq(atkTr_or(d2_sup,d3_sup), atkTr_or(atkTr_or(d4_sup,d8_sup),atkTr_or(d5_sup,atkTr_or(d6_sup,d7_sup)))));
s1_inf=atkTr_seq(d1_inf, atkTr_seq(atkTr_or(d2_inf,d3_inf), atkTr_or(atkTr_or(d4_inf,d8_inf),atkTr_or(d5_inf,atkTr_or(d6_inf,d7_inf)))));
s4_sup=atkTr_seq(d12_sup,d13_sup);
s4_inf=atkTr_seq(d12_inf,d13_inf);
s2_sup=atkTr_seq(atkTr_seq(d9_sup,atkTr_or(atkTr_seq(d10_sup,d11_sup), s4_sup)), d14_sup);
s2_inf=atkTr_seq(atkTr_seq(d9_inf,atkTr_or(atkTr_seq(d10_inf,d11_inf), s4_inf)), d14_inf);
s3_sup=atkTr_seq(s1_sup,s2_sup);
s3_inf=atkTr_seq(s1_inf,s2_inf);
cumS3_inf=tlist(['distribution','state','probability'],s3_inf.state,cumsum(s3_inf.probability));
cumS3_sup=tlist(['distribution','state','probability'],s3_sup.state,cumsum(s3_sup.probability));
s2b_sup=atkTr_seq(atkTr_seq(d9_sup,atkTr_or(atkTr_seq(e10_sup,d11_sup), s4_sup)), d14_sup);
s2b_inf=atkTr_seq(atkTr_seq(d9_inf,atkTr_or(atkTr_seq(e10_inf,d11_inf), s4_inf)), d14_inf);
s3b_sup=atkTr_seq(s1_sup,s2b_sup);
s3b_inf=atkTr_seq(s1_inf,s2b_inf);
cumS3b_inf=tlist(['distribution','state','probability'],s3b_inf.state,cumsum(s3b_inf.probability));
cumS3b_sup=tlist(['distribution','state','probability'],s3b_sup.state,cumsum(s3b_sup.probability));
// plot (Fig. 7)
scf(2)
clf()
a=gca();
a.thickness=2;
plot2d2(cumS3_inf.state,cumS3_inf.probability,color('red'));
plot2d2(cumS3_sup.state,cumS3_sup.probability,color('green'));
plot2d2(cumS3b_inf.state,cumS3b_inf.probability,color('blue'));
plot2d2(cumS3b_sup.state,cumS3b_sup.probability,color('magenta'));
legends(['inf';'sup';'double rate collect data inf';'double rate collec data sup'],[color('red') color('green') color('blue') color('magenta')],opt="lr");
