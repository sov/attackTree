mode(-1);
lines(0);
setenv('DEBUG_SCILAB_DYNAMIC_LINK','YES')
try
catch
 error(gettext('Scilab 5.0 or more is required.'));  
 getversion('scilab');
end;
// ====================================================================
if ~with_module('development_tools') then
  error(msprintf(gettext("%s module not installed."),'development_tools'));
end
// ====================================================================
TOOLBOX_NAME = 'attackTree';
TOOLBOX_TITLE = 'Attack Tree';
// ====================================================================
toolbox_dir = get_absolute_file_path('builder.sce');

tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
//tbx_builder_gateway(toolbox_dir);
//help_from_sci("macros/atkTr_and.sci" ,"help/en_US","demos");
//help_from_sci("macros/atkTr_or.sci" ,"help/en_US","demos");
//help_from_sci("macros/atkTr_seq.sci" ,"help/en_US","demos");
//help_from_sci("macros/atkTr_erlangBounds.sci" ,"help/en_US","demos");
//ilib_for_link('heapSortSupport','src/c/heapSortSupport.c',[],"c")
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
// ====================================================================
