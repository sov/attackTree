## Attack Tree scilab module


This project is a scilab external module based on macros. 
It was written with scilab 5.5.2.
It contains various functions for quantitative attack tree analysis 
with stochastic bounds.


To use it, from scilab, in the module directory, execute the following files :

```
exec('builder.sce')
exec('loader.sce')
```

The html documentation is built in the directory `help/en_US/scilab_en_US_help/`.
An out of date copy of the documentation can be browsed through https://www.lacl.fr/tan/attackTree/scilab_en_US_help/.

This work was partially supported by grant ANR MARMOTE (ANR-12-MONU-0019).
